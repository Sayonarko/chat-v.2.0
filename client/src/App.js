import Header from "./components/header";
import Main from "./components/main";
import Preloader from "./components/preloader";
import Notifications from "./components/notifications";

import './App.scss';

function App() {
  return (
    <div className="App">
      <Header />
      <Main />
      <Preloader />
      <Notifications />
    </div>
  );
}

export default App;
