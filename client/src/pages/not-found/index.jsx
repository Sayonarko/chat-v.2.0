import React from "react";
import { NavLink } from "react-router-dom";
import { ROUTER } from "../../common/enums/app-routes";
import "./style.scss";

export default function NotFoundPage() {

  return (
    <div className="not-found-page">
      <h1 className="not-found-page-title">Oops... Page not found</h1>
      <NavLink className="not-found-page-link" to={ROUTER.chat}>Back to chat</NavLink>
    </div>
  )
}