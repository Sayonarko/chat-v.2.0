import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { setProfile } from "../../redux/actions";

import "./style.scss";

export default function LoginPage() {

  const dispatch = useDispatch();
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  const onLogin = (e) => {
    e.preventDefault();
    const payload = {
      username,
      password
    };
    dispatch(setProfile(payload));
  };


  return (
    <div className="login-page">
      <div className="login-container">
        <h1 className="login-title">Welcome back!</h1>
        <form className="login-form" onSubmit={onLogin}>
          <input
            className="login-input"
            type="text"
            name="username"
            placeholder="username"
            value={username}
            autoComplete={username}
            onChange={e => setUsername(e.target.value)} />
          <input
            className="login-input"
            type="password"
            name="password"
            placeholder="password"
            value={password}
            autoComplete={password}
            onChange={e => setPassword(e.target.value)} />
          <button
            className="login-button"
            type="submit">
            login
          </button>
        </form>
      </div>
    </div>
  )
}