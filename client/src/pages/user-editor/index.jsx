import React, { useCallback, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { deleteUser, getAllUsers, editUser as editUserAction, addUser } from "../../redux/actions";
import UserCardEdit from "../../components/user-card-edit";
import EditModal from "../../components/edit-modal";

import "./style.scss";

export default function UserEditorPage() {
  const [editUser, setEditUser] = useState(null);
  const [isAddUser, setIsAddUser] = useState(false);
  const dispatch = useDispatch();
  const { users, profile } = useSelector(state => ({
    users: state.users.users,
    profile: state.profile.user
  }));

  const handleDeleteUser = useCallback((userId) => {
    dispatch(deleteUser(userId));
  }, [dispatch]);

  const handleEditUser = useCallback((user) => {
    dispatch(editUserAction(user));
    dispatch(getAllUsers());
  }, [dispatch]);

  const handleAddUser = useCallback((user) => {
    dispatch(addUser(user));
  }, [dispatch]);

  const changeEditUser = (user) => setEditUser(user);

  const onAddUser = () => {
    setIsAddUser(true);
    setEditUser(true);
  };

  const onClose = () => {
    setIsAddUser(false);
    setEditUser(null);
  };

  useEffect(() => {
    dispatch(getAllUsers());
  }, [dispatch]);

  return (
    <div className="user-list-page">
      {editUser && <EditModal
                      isAddUser={isAddUser}
                      user={editUser}
                      handleEditUser={handleEditUser}
                      handleAddUser={handleAddUser}
                      onClose={onClose}
      />}
      <h1 className="user-list-title">All users</h1>
      <div className="user-list-buttons">
        <button
          className="add-user-button"
          onClick={onAddUser}>
          Add user
        </button>
      </div>
      <div className="users">
        {users.map(user => <UserCardEdit
                              key={user.userId}
                              user={user}
                              handleDeleteUser={handleDeleteUser}
                              changeEditUser={changeEditUser}
                              profile={profile} />)}
      </div>
    </div>
  )
}