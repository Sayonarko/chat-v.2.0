import React from "react";
import "./style.scss";

export default function UserCardEdit({ user, handleDeleteUser, changeEditUser, profile }) {
  const { username, userId } = user;
  const disabled = username === profile.username;

  const onDeleteUser = () => handleDeleteUser(userId);
  const onChangeEditUser = () => changeEditUser(user);
  return (
    <div className="user-card-edit">
      <h2 className="user-card-edit-title">{username}</h2>
      <button
        className="edit-button"
        onClick={onChangeEditUser}>edit</button>
      <button
        className="delete-button"
        disabled={disabled}
        onClick={onDeleteUser}>
        delete
      </button>
    </div>
  )
}