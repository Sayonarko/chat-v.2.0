import React from "react";
import { CircularProgress } from "@material-ui/core";
import { useSelector } from "react-redux";

import "./style.scss";

export default function Preloader() {
  const { isLogin, isLoadMessages, isLoadUsers } = useSelector(state => ({
    isLogin: state.profile.preloader,
    isLoadMessages: state.chat.preloader,
    isLoadUsers: state.users.preloader
  }));

  const isLoading = isLogin || isLoadMessages || isLoadUsers;

  return (
    <div className="preloader" style={{ display: isLoading ? "flex" : "none" }}>
      <CircularProgress size="100px" />
    </div>
  )
}