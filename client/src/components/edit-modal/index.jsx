import React, { useState, useEffect, useRef } from "react";
import "./style.scss";

export default function EditModal({ user, handleEditUser, handleAddUser, onClose, isAddUser }) {
  const { username, avatar, email, roles } = user;
  const [userValue, setUserValue] = useState({
    username: "",
    avatar: "",
    email: "",
    roles: "user"
  });
  const modalRef = useRef();
  console.log(userValue);
  const changeValue = (e) => {
    setUserValue({
      ...userValue,
      [e.target.name]: e.target.value
    });
  };

  const onClickClose = e => (e.target === modalRef.current) && onClose();

  const editUser = (e) => {
    e.preventDefault();
    const editedUser = {
      ...user,
      ...userValue,
      roles: [userValue.roles],
      editedAt: Date.now()
    };
    handleEditUser(editedUser);
    onClose();
  };

  const onAddUser = (e) => {
    e.preventDefault();
    const newUser = {
      ...userValue,
      roles: [userValue.roles],
      userId: Date.now().toString(),
      createdAt: Date.now(),
      editedAt: ""
    };
    handleAddUser(newUser);
    onClose();
  };

  useEffect(() => {
    if (!isAddUser) {
      setUserValue({
        username,
        avatar,
        email,
        roles
      });
    }
  }, [isAddUser, username, avatar, email, roles]);


  return (
    <div
      ref={modalRef}
      onClick={onClickClose}
      className="edit-user-modal">
      <div className="edit-user-modal-body">
        <h2 className="edit-user-title">{isAddUser ? "Add user" : "Edit user"}</h2>
        <form className="edit-user-form" onSubmit={isAddUser ? onAddUser : editUser}>
          <label>
            Username
            <input
              className="edit-user-input"
              name="username"
              value={userValue.username}
              onChange={changeValue} />
          </label>
          <label>
            Avatar
            <input
              className="edit-user-input"
              name="avatar"
              value={userValue.avatar}
              onChange={changeValue} />
          </label>
          <label>
            E-mail
            <input
              className="edit-user-input"
              name="email"
              value={userValue.email}
              onChange={changeValue} />
          </label>
          {isAddUser && <label>
            password
            <input
              className="edit-user-input"
              name="password"
              value={userValue?.password}
              onChange={changeValue} />
          </label>}
          <label>
            Role
            <br />
            <select
              name="roles"
              className="edit-user-select"
              onChange={changeValue}>
              <option>user</option>
              <option>admin</option>
            </select>
          </label>

          <div className="edit-user-buttons">
            <button
              className="edit-user-button"
              type="submit">
              ok
            </button>
            <button
              className="edit-user-close"
              type="button"
              onClick={onClose}
            >
              cancel
            </button>
          </div>
        </form>
      </div>
    </div >
  )
}

