import React, { useRef, useEffect, useState } from "react";
import Message from "../message";
import OwnMessage from "../own-message";
import Divider from "../divider";
import { useSelector } from "react-redux";

import "./style.scss";

export default function MessageList({ handleDeleteMessage }) {

  const { messages, username } = useSelector(state => ({
    messages: state.chat.messages,
    username: state.profile.user.username
  }));

  const [groupedMessages, setGroupedMessages] = useState([]);
  const listRef = useRef();

  function groupMessages(messages) {
    const map = new Map();

    messages.forEach(message => {
      const messsageDate = new Date(message.createdAt).getDate();
      const messsageMonth = new Date(message.createdAt).toLocaleString("en", { month: "long" });
      const messageDay = new Date(message.createdAt).toLocaleString("en", { weekday: "long" });
      const date = `${messageDay}, ${messsageDate} ${messsageMonth}`;

      if (map.has(date)) {
        const day = map.get(date);
        map.set(date, [...day, message]);
      } else {
        map.set(date, [message]);
      }
    })
    return Array.from(map.entries());
  }

  useEffect(() => {
    listRef.current.scrollTop = listRef.current.scrollHeight;
  }, [groupedMessages]);

  useEffect(() => {
    setGroupedMessages(groupMessages(messages));
  }, [messages]);

  return (
    <div ref={listRef} className="message-list">

      {groupedMessages.map(([date, messages]) => {
        return (
          <div key={date}>
            <Divider date={date} />
            {messages.map(message => {
              if (message.user === username) {
                return <OwnMessage
                  key={message.id}
                  message={message}
                  handleDeleteMessage={handleDeleteMessage} />
              }
              return <Message key={message.id} message={message} />
            })}
          </div>
        )
      })}
    </div>
  )
}