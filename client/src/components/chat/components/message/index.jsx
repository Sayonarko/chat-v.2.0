import React, { useState } from "react";
import { getTime } from "../../../../helpers";

import "./style.scss";

export default function Message({ message }) {
  const { avatar, text, user, editedAt, createdAt } = message;
  const formattedTime = editedAt ? getTime(editedAt) : getTime(createdAt);
  const [isLike, setIslike] = useState(false);

  const handlelike = () => setIslike(!isLike);

  return (
    <div className="message">
      <div className="message-user-avatar">
        <img src={avatar} alt="" />
      </div>
      <div className="message-body">
        <h2 className="message-user-name">{user}</h2>
        <div className="message-text">{text}</div>
        <div className="message-body-bottom">
          <div className="message-time">{formattedTime}</div>
          <button
            className={isLike ? "message-liked" : "message-like"}
            onClick={handlelike} >
            <svg xmlns="http://www.w3.org/2000/svg" stroke="white" fill="none" width="25" height="25"
              viewBox="-2 -3 20 20" >
              <path fillRule="evenodd" d="M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z" />
            </svg>
          </button>
        </div>
      </div>
    </div>
  )
}