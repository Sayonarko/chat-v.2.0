import React from "react";
import { useSelector } from "react-redux";
import { getFullDate, getUniqueUsers } from "../../../../helpers";

import "./style.scss";

export default function Header() {

  const messages = useSelector(state => state.chat.messages);
  const usersCount = getUniqueUsers(messages);
  const messagesCount = messages.length;
  const lastMessageDate = messages[messages.length - 1]?.createdAt
  const formattedLastMessageDate = lastMessageDate ? getFullDate(lastMessageDate) : "No messages";

  return (
    <header className="header">
      <h1 className="header-title">My chat</h1>
      <div>
        <span className="header-users-count">{usersCount}</span>
        {" "}
        participants
      </div>
      <div>
        <span className="header-messages-count">{messagesCount}</span>
        {" "}
        messages
      </div>
      <div className="header-last-message">
        Last message at
        {" "}
        <span className="header-last-message-date">{formattedLastMessageDate}</span>
      </div>
    </header>
  )
}