import React, { useEffect } from "react";
import { useHistory } from "react-router-dom";
import Header from "./components/header";
import MessageList from "./components/message-list";
import MessageInput from "./components/messasge-input";
import { useDispatch, useSelector } from "react-redux";
import { getAllMessages, addMessage, deleteMessage } from "../../redux/actions";
import { ROUTER } from "../../common/enums/app-routes";

export default function Chat() {
  const history = useHistory();
  const dispatch = useDispatch();
  const { messages, profile } = useSelector(state => ({
    messages: state.chat.messages,
    profile: state.profile.user
  }));

  const handleAddMessage = message => dispatch(addMessage(message));

  const handleDeleteMessage = id => dispatch(deleteMessage(id));

  const editLastOwnMessage = (e) => {

    if (e.code === "ArrowUp") {
      const ownMessages = messages.filter(message => message.user === profile.username);
      const lastOwnMessage = ownMessages.pop();
      if (lastOwnMessage) history.push(ROUTER.messageEditor_$ID.replace(":id", lastOwnMessage.id));
    }
  }

  useEffect(() => {
    dispatch(getAllMessages());
  }, [dispatch]);

  return (
    <div className="chat" onKeyUp={editLastOwnMessage} >
      <Header />
      <MessageList handleDeleteMessage={handleDeleteMessage} />
      <MessageInput handleAddMessage={handleAddMessage} profile={profile} />
    </div>
  )
}