import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { resetError } from "../../redux/actions";
import Snackbar from '@material-ui/core/Snackbar';
import Alert from '@material-ui/lab/Alert';
import "./style.scss";

export default function Notifications() {
  const {
    profileError,
    usersError,
    chatError,
    profileErrorMessage,
    usersErrorMessage,
    chatErrorMessage } = useSelector(state => ({
      profileError: state.profile.error.status,
      usersError: state.users.error.status,
      chatError: state.chat.error.status,
      profileErrorMessage: state.profile.error.errorMessage,
      usersErrorMessage: state.users.error.errorMessage,
      chatErrorMessage: state.chat.error.errorMessage,
    }));
  const isError = profileError || usersError || chatError;
  const errorMessage = profileErrorMessage || usersErrorMessage || chatErrorMessage;
  const dispatch = useDispatch();

  const onClose = () => dispatch(resetError());

  return (
    <Snackbar
      open={isError}
      onClose={onClose}
      autoHideDuration={6000}
      onClick={onClose}
      anchorOrigin={{ vertical: 'top', horizontal: 'right' }}>
      <Alert severity="error">{errorMessage}</Alert>
    </Snackbar>
  );
}