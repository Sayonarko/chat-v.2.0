import React from "react";
import { NavLink } from "react-router-dom";
import { ROUTER } from "../../common/enums/app-routes";

import "./style.scss";

export default function Menu() {

  return (
    <nav className="menu">
      <NavLink
        className="menu-link"
        to={ROUTER.chat}>Chat</NavLink>
      <NavLink
        className="menu-link"
        to={ROUTER.userList}>User list</NavLink>
      <NavLink
        className="menu-link"
        to={ROUTER.userEditor}>User editor</NavLink>
    </nav>
  )
}