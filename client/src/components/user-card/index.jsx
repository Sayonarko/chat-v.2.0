import React from "react";
import { getFullDate } from "../../helpers";
import "./style.scss";

export default function UserCard({ user }) {
  const { username, createdAt, avatar, email } = user;

  return (
    <div className="user-card">
      <div className="avatar">
        <img src={avatar} alt="" />
      </div>
      <div className="body">
        <h2 className="username">{username}</h2>
        <div>
          E-mail:
          {" "}
          <span>{email}</span>
        </div>
        <div>
          First login:
          {" "}
          <span>{getFullDate(createdAt)}</span>
        </div>
      </div>
    </div>
  )
}