import React from "react";
import logo from "../../img/logo.svg";
import Menu from "../menu";
import { useSelector } from "react-redux";

import "./style.scss";

export default function Header() {
  const user = useSelector(state => state.profile.user);
  const userIsAdmin = user ? user.roles.includes("admin") : false;

  return (
    <div className="main-header">
      <a href="/">
        <img src={logo} alt="logo" />
      </a>
      {userIsAdmin && <Menu />}
    </div>
  )
}