export const errorMessages = {
  FAILED_LOAD_MESSAGES: "Failed to load messages",
  FAILED_LOAD_USERS: "Failed to load users",
  FAILED_LOGIN: "Failed to login",
}