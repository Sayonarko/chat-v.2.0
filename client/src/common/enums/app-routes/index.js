export const ROUTER = {
  homepage: "/",
  login: "/login",
  chat: "/chat",
  messageEditor_$ID: "/message-editor/:id",
  userEditor: "/user-editor",
  userList: "/user-list"
}