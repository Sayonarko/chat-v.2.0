import { Http } from "./http/http.service";
import { Auth } from "./auth/auth.service";
import { Message } from "./message/message.service";
import { User } from "./user/user.service";

const http = new Http();
const auth = new Auth({ http });
const user = new User({ http });
const message = new Message({ http });

export { http, auth, user, message };
