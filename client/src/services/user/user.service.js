import { ContentType, HttpMethod } from '../../common/enums/http';

class User {
  constructor({ http }) {
    this._http = http;
  }

  getAllUsers() {
    return this._http.load('/api/users', {
      method: HttpMethod.GET
    });
  }

  getUserById(id) {
    return this._http.load(`/api/users/${id}`, {
      method: HttpMethod.GET
    });
  }

  addUser(payload) {
    return this._http.load('/api/users', {
      method: HttpMethod.POST,
      contentType: ContentType.JSON,
      payload: JSON.stringify(payload)
    });
  }

  editUser(payload) {
    return this._http.load('/api/users', {
      method: HttpMethod.PUT,
      contentType: ContentType.JSON,
      payload: JSON.stringify(payload)
    });
  }

  deleteUser(id) {
    return this._http.load(`/api/users/${id}`, {
      method: HttpMethod.DELETE
    });
  }
}

export { User };
