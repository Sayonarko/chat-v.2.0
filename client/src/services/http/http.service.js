import { HttpHeader, HttpMethod } from '../../common/enums/http';
import { API_URL } from "../../common/enums/env/env.enum";

class Http {

  load(url, options = {}) {
    const {
      method = HttpMethod.GET,
      payload = null,
      contentType,
    } = options;
    const headers = this._getHeaders({
      contentType
    });

    return fetch(API_URL + url, {
      method,
      headers,
      body: payload
    })
      .then(this._checkStatus)
      .then(this._parseJSON)
      .catch(this._throwError);
  }

  _getHeaders({ contentType }) {
    const headers = new Headers();

    if (contentType) {
      headers.append(HttpHeader.CONTENT_TYPE, contentType);
    }

    return headers;
  }

  async _checkStatus(response) {
    if (!response.ok) {
      const parsedException = await response.json();

      throw new Error(parsedException?.message ?? response.statusText);
    }

    return response;
  }

  _parseJSON(response) {
    return response.json();
  }

  _throwError(err) {
    throw err;
  }
}

export { Http };
