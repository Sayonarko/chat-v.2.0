export function getFullDate(date) {
  const year = new Date(date).getFullYear();
  let month = new Date(date).getMonth() + 1;
  let day = new Date(date).getDate();
  const hours = new Date(date).getHours();
  let minutes = new Date(date).getMinutes();

  if (month < 10) {
    month = "0" + month;
  }

  if (day < 10) {
    day = "0" + day;
  }

  if (minutes < 10) {
    minutes = "0" + minutes;
  }

  return `${day}.${month}.${year} ${hours}:${minutes}`;
}

export function getTime(date) {
  let hours = new Date(date).getHours();
  let minutes = new Date(date).getMinutes();

  if (hours < 10) {
    hours = "0" + hours;
  }

  if (minutes < 10) {
    minutes = "0" + minutes;
  }

  return `${hours}:${minutes}`;
}

export function getUniqueUsers(messages) {
  const users = messages.map(message => message.user);
  const uniqueUsers = new Set(users);

  return uniqueUsers.size;
}