import chatReducer from "./chat/chat.slice";
import profileReducer from "./profile/profile.slice";
import usersReducer from "./users/users.slice";

export { chatReducer, profileReducer, usersReducer };