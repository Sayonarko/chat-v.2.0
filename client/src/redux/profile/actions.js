import { createAsyncThunk, createAction } from "@reduxjs/toolkit";
import { auth as authService } from "../../services";
import { SET_PROFILE, RESET_ERROR } from "./action-types";

const setProfile = createAsyncThunk(
  SET_PROFILE,
  async (payload) => await authService.login(payload)
);

const resetError = createAction(RESET_ERROR);

export { setProfile, resetError };