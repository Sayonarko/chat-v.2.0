import { createAsyncThunk, createAction } from "@reduxjs/toolkit";
import { message as messageService } from "../../services";
import { GET_MESSAGES, ADD_MESSAGE, EDIT_MESSAGE, DELETE_MESSAGE, RESET_ERROR } from "./action-types";

const getAllMessages = createAsyncThunk(
  GET_MESSAGES,
  async () => await messageService.getAllMessages()
);

const addMessage = createAsyncThunk(
  ADD_MESSAGE,
  async (message) => await messageService.addMessage(message)
);

const editMessage = createAsyncThunk(
  EDIT_MESSAGE,
  async (message) => await messageService.editMessage(message)
);

const deleteMessage = createAsyncThunk(
  DELETE_MESSAGE,
  async (id) => await messageService.deleteMessage(id)
);

const resetError = createAction(RESET_ERROR);


export { getAllMessages, addMessage, editMessage, deleteMessage, resetError };