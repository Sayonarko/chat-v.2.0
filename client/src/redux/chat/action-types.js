export const SHOW_PRELOADER = "preloader/show-preloader";
export const HIDE_PRELOADER = "preloader/hide-preloader";

export const GET_MESSAGES = "messages/get-messages";
export const ADD_MESSAGE = "messages/add-message";
export const EDIT_MESSAGE = "messages/edit-message";
export const DELETE_MESSAGE = "messages/delete-message";

export const RESET_ERROR = "messages/reset-error";