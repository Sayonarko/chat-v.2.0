import { createSlice } from "@reduxjs/toolkit";
import { getAllUsers, addUser, editUser, deleteUser, resetError } from "./actions";
import { errorMessages } from "../../common/enums/errors/errors.enum";

const initialState = {
  users: [],
  preloader: false,
  error: {
    status: false,
    errorMessage: ""
  }
};

const usersSlice = createSlice({
  name: "users",
  initialState,
  reducers: {},
  extraReducers: {
    [getAllUsers.pending]: (state, action) => {
      state.preloader = true;
      state.error.status = false;
      state.error.errorMessage = "";
    },
    [getAllUsers.fulfilled]: (state, action) => {
      state.preloader = false;
      state.users = action.payload;
    },
    [getAllUsers.rejected]: (state, action) => {
      state.preloader = false;
      state.error.status = true;
      state.error.errorMessage = errorMessages.FAILED_LOAD_USERS;
    },
    [addUser.fulfilled]: (state, action) => {
      state.users.push(action.payload);
    },
    [editUser.fulfilled]: (state, action) => {
      const user = action.payload;

      state.users = state.users.map(oldUser => {
        if (oldUser.userId === user.userId) return user;
        return oldUser;
      })
    },
    [deleteUser.fulfilled]: (state, action) => {
      const id = action.payload;

      state.users = state.users.filter(user => user.userId !== id);
    },
    [resetError]: (state) => {
      state.error.status = false;
      state.error.errorMessage = "";
    }
  },
})

export default usersSlice.reducer;