export const GET_USERS = "users/get-users";
export const ADD_USER = "users/add-user";
export const EDIT_USER = "users/edit-user";
export const DELETE_USER = "users/delete-user";

export const RESET_ERROR = "users/reset-error";