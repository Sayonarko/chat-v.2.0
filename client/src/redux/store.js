import { configureStore, getDefaultMiddleware } from '@reduxjs/toolkit';
import { chatReducer, profileReducer, usersReducer } from "./root-reducer";

const store = configureStore({
  reducer: {
    profile: profileReducer,
    chat: chatReducer,
    users: usersReducer
  },
  middleware: getDefaultMiddleware({ thunk: true }),
});

export default store;