import Chat from "./src/components/chat";
import rootReducer from "./src/redux/reducers";

// eslint-disable-next-line import/no-anonymous-default-export
export default {
  Chat,
  rootReducer
};