import { Router } from "express";
import db from "../config/db.js";

const router = Router();

router.get("/", (req, res) => {
  try {
    const messages = db.data.messages;
    res.send(messages);
  } catch (err) {
    res.err = err;
  }
});

router.post("/", async (req, res) => {
  try {
    const message = req.body;

    db.data.messages.push(message);
    await db.write();

    res.send(message);
  } catch (err) {
    res.err = err;
  }
});

router.put("/", async (req, res) => {
  try {
    const message = req.body;

    db.data.messages = db.data.messages.map(oldMessage => {
      if (oldMessage.id === message.id) return message;
      return oldMessage;
    });

    await db.write();
    res.json(message);
  } catch (err) {
    res.err = err;
  }
});

router.delete("/:id", async (req, res) => {
  try {
    const id = req.params.id;
    db.data.messages = db.data.messages.filter(message => message.id !== id);

    await db.write();
    res.json(id);
  } catch (err) {
    res.err = err;
  }
});


export default router;