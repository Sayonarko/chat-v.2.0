import userRoutes from "./user.routes.js";
import messagesRoutes from "./messages.routes.js";
import authRoutes from "./auth.routes.js";

export default (app) => {
  app.use("/api/auth/login", authRoutes);
  app.use("/api/users", userRoutes);
  app.use("/api/messages", messagesRoutes);
}