import { Router } from "express";
import db from "../config/db.js";

const router = Router();

router.post("/", (req, res) => {
  try {
    const { username, password } = req.body;
    const user = db.data.users.find(user => user.username === username);
    if (user) {
      if (user.password === password) {
        const { username, avatar, userId, roles, email } = user;
        res.send({
          username,
          avatar,
          userId,
          email,
          roles
        });
      } else {
        res.sendStatus(403);
      }
    } else {
      res.sendStatus(404);
    }

  } catch (err) {
    res.err = err;
  }
});

export default router;