import { Router } from "express";
import db from "../config/db.js";

const router = Router();

router.get("/", (req, res) => {
  try {
    const users = db.data.users;
    res.json(users);
  } catch (err) {
    res.err = err;
  }
});

router.get("/:id", (req, res) => {
  try {
    const id = req.params.id;
    const user = db.data.users.find(user => user.userId === id);
    if (user) {
      res.json(user);
    } else {
      res.sendStatus(404);
    }
  } catch (err) {
    res.err = err;
  }
});

router.put("/", async (req, res) => {
  try {
    const user = req.body;
    db.data.users = db.data.users.map(oldUser => {
      if (oldUser.userId === user.userId) return user;
      return oldUser;
    });
    await db.write();
    res.json(user);
  } catch (err) {
    res.err = err;
  }
});

router.post("/", async (req, res) => {
  try {
    const user = req.body;
    db.data.users.push(user);
    await db.write();
    res.json(user);
  } catch (err) {
    res.err = err;
  }
});

router.delete("/:id", async (req, res) => {

  try {
    const id = req.params.id;

    db.data.users = db.data.users.filter(user => user.userId !== id);
    await db.write();
    res.json(id);
  } catch (err) {
    res.err = err;
  }
});

export default router;