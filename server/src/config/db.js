import { Low, JSONFile } from 'lowdb'
import { join, resolve } from 'path';

const __dirname = resolve();
const file = join(__dirname, '/src/db/db.json');
const adapter = new JSONFile(file);
const db = new Low(adapter);

db.read();
db.write();

export default db;
